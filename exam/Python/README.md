# Library database

![Library database](libdb.png)

Translation from danish:

- "Vis hele listen" = Show all elements in the database
- "Søgestreng:" and "Søg i listen" = Search for elements
- "ID for udlån:" and "Udlån" = Chose the element you want to borrow, based on it's ID
- "ID for aflevering:" and "Aflever" = Return an element, based on it's ID
- "ID for sletning:" and "Slet" = Remove element from database

There will be error messages if you try to do something that shouldn't be possible. For instance borrowing an element that's not in the database anymore (for instance if it's been deleted, or all examples already borrowed). 