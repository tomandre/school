from tkinter import *
from tkinter import messagebox
from klasser import *

# TODO - du skal have initialiseret nogle bogobjekter og nogle filmobjekter
# og sat dem ind i listen. Lav minimum tre af hver type -
# og husk at give dem alle et forskelligt ID - som skal bruges til
# at udlåne det.

list_materialer = [
    Bog(1, "Oprindelse", 2, 1, 2017, 555, "Dan Brown"),
    Bog(2, "Flagermusmanden", 3, 1, 1997, 357, "Jo Nesbø"),
    Bog(3, "Eliteserien", 5, 2, 2008, 208, "Frode Øverli"),
    Film(4, "The Station Agent", 3, 1, 2003, "Tom McCarthy", 90),
    Film(5, "The Matrix", 3, 0, 1999, "The Wachowski Brothers", 136),
    Film(6, "Finding Nemo", 5, 4, 2003, "Andrew Stanton", 100),
    Cd(7, "Doolittle", 2, 0, 1989, "Pixies", 39),
    Cd(8, "Industrial Silence", 1, 1, 1999, "Madrugada", 64),
    Cd(9, "When I'm Free", 3, 2, 2015, "Ane Brun", 46)
    # nedenunder er der er objekt der er kommenteret ud, 
    # men det ved at inkludere det demonstreres det at klasserene 
    # BOG, Film og Cd ikke blot nedarver fra Materialer, men også 
    # overskriver: 
    # Materiale(10,"materialedummy", 1, 0, 1900)
    ]

class Application(Frame):

    def udlaan(self):
        idnr = self.id_entry.get()
        print("id der skal lånes: " + idnr)
        # TODO - her skal du have udlånt det korrekte materiale
        # med det korrekte id og opdatere objektet.
        # Der er taget højde for at materialet er tilgængelig.
        # For at sikre at vi gennemgår listen i korrekt rækkefølge,
        # er der en variabel vis_resultat der sættes til True når 
        # vi skal præsentere noget i brugergrænsefladen. Hvis denne variabel 
        # aldrig når at bliver True, går vi videre til en fejlbesked.
        vis_resultat = False
        if idnr != "":
            for materialer in list_materialer:
                if int(idnr) == materialer.idnr:
                    vis_resultat = True
                    if materialer.antaludlaan < materialer.antal and materialer.antal > 0:
                        materialer.antaludlaan += 1
                    elif materialer.antal == 0:
                        messagebox.showinfo("Kan ikke udlånes", f"Vi har desværre ikke {materialer.titel} på lager")
                    else:
                        messagebox.showinfo("Kan ikke udlånes", f"Alle {materialer.titel} er desværre udlånt")
                    self.listGui.delete("1.0", END)
                    for materialer in list_materialer:
                        self.listGui.insert(INSERT, materialer.toString() + "\n")
        else:
            vis_resultat = True
            self.listGui.delete("1.0", END)
            self.listGui.insert(INSERT, "Du har ikke tastet ind noget, udlån er ikke mulig")
        if not vis_resultat:
            messagebox.showinfo("ID eksiterer ikke", f"Du har tastet ind {idnr}. Det er desværre forkert")

    def aflever(self):
        idnr = self.aflever_entry.get()
        print("id der skal afleveres: " + idnr)
        # TODO - her skal du have afleveret det korrekte materiale
        # med det korrekte id og så opdatere objektet.
        # Der er taget højde for at materialet faktisk kan afleveres.
        # For at sikre at vi gennemgår listen i korrekt rækkefølge,
        # er der en variabel vis_resultat der sættes til True når 
        # vi skal præsentere noget i brugergrænsefladen. Hvis denne variabel 
        # aldrig når at bliver True, går vi videre til en fejlbesked.
        vis_resultat = False
        if idnr != "":
            for materialer in list_materialer:
                if int(idnr) == materialer.idnr:
                    vis_resultat = True
                    if materialer.antaludlaan >= 1:
                        materialer.antaludlaan -= 1
                    else:
                        messagebox.showinfo("Kan ikke afleveres", f"Vi har alle vores {materialer.titel}, er det din egen?")
                    
                    self.listGui.delete("1.0", END)
                    for materialer in list_materialer:
                        self.listGui.insert(INSERT, materialer.toString() + "\n")
        else:
            vis_resultat = True
            self.listGui.delete("1.0", END)
            self.listGui.insert(INSERT, "Du har ikke tastet ind noget, aflevering er ikke mulig")
        if not vis_resultat:
            messagebox.showinfo("ID eksiterer ikke", f"Du har tastet ind {idnr}. Det er desværre forkert")
 
    def slet(self):
        idnr = self.slet_entry.get()
        print("id der skal slettes: " + idnr)
        # Dette er en ekstra funktion for at slette én styk af et materiale.
        # Der er taget højde for der ikke forsøges at slette noget der ikke 
        # kan slettes. Fx. fordi der er pt. er 0 styk af materialet på lager. 
        # Eller fordi de resterende materialer er udlånt.
        # For at sikre at vi gennemgår listen i korrekt rækkefølge,
        # er der en variabel vis_resultat der sættes til True når 
        # vi skal præsentere noget i brugergrænsefladen. Hvis denne variabel 
        # aldrig når at bliver True, går vi videre til en fejlbesked.
        vis_resultat = False
        if idnr != "":
            for materialer in list_materialer:
                if int(idnr) == materialer.idnr:
                    vis_resultat = True
                    if materialer.antal > 0 and materialer.antal - materialer.antaludlaan > 0 :
                        materialer.antal -= 1
                    elif materialer.antal > 0 and materialer.antal - materialer.antaludlaan <= 0:
                        messagebox.showinfo("Kan ikke slettes", f"Nogen har {materialer.titel} der hjemme")
                    else:
                        messagebox.showinfo("Kan ikke slettes", f"Vi har slettet alle vores {materialer.titel}")
                    
                    self.listGui.delete("1.0", END)
                    for materialer in list_materialer:
                        self.listGui.insert(INSERT, materialer.toString() + "\n")
        else:
            vis_resultat = True
            self.listGui.delete("1.0", END)
            self.listGui.insert(INSERT, "Du har ikke tastet ind noget, sletning er ikke mulig")
        if not vis_resultat:
            messagebox.showinfo("ID eksiterer ikke", f"Du har tastet ind {idnr}. Det er desværre forkert")

    def sog_i_listen(self):
        search_text = self.entry.get()
        print("søge tekst: "+search_text)
        # print(materiale(listMaterialer).titel)
        # TODO Nu skal listen af materiale søges igennem, og
        # de materialer, som matcher (dvs. hvor søgestrengen indgår som
        # en delstring) skal nu vises i listen og altså IKKE alle
        # materialer. Så du kan få brug for at slette det som
        # allerede står i vinduet og så tilføje de materialer,
        # der matcher.
        # For at sikre at vi gennemgår listen i korrekt rækkefølge,
        # er der en variabel vis_resultat der sættes til True når 
        # vi skal præsentere noget i brugergrænsefladen. Hvis denne variabel 
        # aldrig når at bliver True, går vi videre til en fejlbesked.
        self.listGui.delete('1.0', END)
        vis_resultat = False
        if search_text != "":
            for materialer in list_materialer:
                if search_text.lower() in materialer.titel.lower(): 
                    vis_resultat = True
                    self.listGui.insert(INSERT, materialer.toString() + "\n")
        else:
            vis_resultat = True
            self.listGui.insert(INSERT, "Du har ikke tastet ind noget, søgning er ikke mulig")
        if not vis_resultat:
            self.listGui.insert(INSERT, f"Desværre kan vi ikke finde noget der matcher {search_text}. Prøv at søge efter noget andet.")

    def vis_hele_listen(self):
        print("Vis hele listen")

        # Linjen nedenunder sletter hele listen i GUI'en
        # Den være være nyttig andre steder.....
        self.listGui.delete('1.0', END)
        # TODO - nu skal du vise HELE listen af materialer igen
        for materialer in list_materialer:
            self.listGui.insert(INSERT, materialer.toString() + "\n")


    def create_widgets(self):
        frame = Frame(self)
        self.winfo_toplevel().title("Biblioteks databasen")

        # definition af quit-knap
        self.QUIT = Button(frame, text="QUIT")
        self.QUIT["fg"] = "red"
        self.QUIT["command"] = self.quit
        self.QUIT.pack({"side": "left"})

        # definition og mapping af vis hele listen knappen
        self.visListe = Button(frame,text="Vis hele listen")
        self.visListe["command"] = self.vis_hele_listen
        self.visListe.pack({"side": "left"})

        # definition af input-søgefeltet.
        self.L1 = Label(frame, text="Søgestreng:")
        self.L1.pack(side=LEFT)
        self.entry = Entry(frame, bd=5)
        self.entry.pack(side=LEFT)

        # definition og mapping af søgeknappen.
        self.sogKnap = Button(frame, text="Søg i listen")
        self.sogKnap["command"] = self.sog_i_listen
        self.sogKnap.pack({"side": "left"})

        # definition af ID inputfeltet til udlån
        self.L1 = Label(frame, text="ID for udlån:")
        self.L1.pack(side=LEFT)
        self.id_entry = Entry(frame, bd=5)
        self.id_entry.pack(side=LEFT)

        # definition af udlånsknappen og mapping til
        # en funktion.
        self.udlaanKnap = Button(frame, text="Udlån")
        self.udlaanKnap["command"] = self.udlaan
        self.udlaanKnap.pack({"side": "left"})

        # inputfelt til aflevering.
        self.L1 = Label(frame, text="ID for aflevering:")
        self.L1.pack(side=LEFT)
        self.aflever_entry = Entry(frame, bd=5)
        self.aflever_entry.pack(side=LEFT)

        # definition og mapping af afleveringsknap
        self.afleverKnap = Button(frame, text="Aflever")
        self.afleverKnap["command"] = self.aflever
        self.afleverKnap.pack({"side": "left"})

        # inputfelt til sletning.
        self.L1 = Label(frame, text="ID for sletning:")
        self.L1.pack(side=LEFT)
        self.slet_entry = Entry(frame, bd=5)
        self.slet_entry.pack(side=LEFT)

        # definition og mapping af afleveringsknap
        self.sletKnap = Button(frame, text="Slet")
        self.sletKnap["command"] = self.slet
        self.sletKnap.pack({"side": "left"})

        # Her definerer vi en Text-widget - dvs
        # den kan indeholde multiple linjer.
        # Idéen er så at hver linje indeholder et styk materiale.
        # Nedenunder kan du se, hvordan listen af materiale løbes
        # igennem og toString-metoden bliver kaldt, så der bliver
        # indsat en ny linje i Text-widgeten
        self.listGui = Text(self, width=143)
        for materiale in list_materialer:
            self.listGui.insert(INSERT, materiale.toString()+"\n")
        frame.pack()
        self.listGui.pack()

    # Denne constructor køres når programmet starter
    # og sørger for at alle vores widgets bliver lavet.
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.create_widgets()


root = Tk()
app = Application(master=root)
app.mainloop()
