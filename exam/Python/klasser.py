class Materiale(object):
    def __init__(self, idnr=0, titel="", antal=0, antaludlaan=0, aarstal=0, kategori=""):
        self.idnr = idnr
        self.titel = titel
        self.antal = antal
        self.antaludlaan = antaludlaan
        self.aarstal = aarstal
        self.kategori = kategori
    
    def toString(self):
        text = f"ID: {self.idnr:^} Titel: {self.titel} Antal: {self.antal} Udlånt: {self.antaludlaan} År: {self.aarstal}"
        return text
     
class Bog(Materiale):
    def __init__(self, idnr=0, titel="", antal=0, antaludlaan=0, aarstal=0, antalsider=0, forfatter="", kategori="Bog"):
        super().__init__(idnr, titel, antal, antaludlaan, aarstal, kategori)
        self.antalsider = antalsider
        self.forfatter = forfatter
 
    def toString(self):
        text = f"ID: {self.idnr:<5} Titel({self.kategori}): {self.titel:<20} Antal: {self.antal:<5} Udlånt: {self.antaludlaan:<5} År: {self.aarstal:<8} Sider: {self.antalsider:<9} Forfatter: {self.forfatter:<}"
        return text

class Film(Materiale):
    def __init__(self, idnr=0, titel="", antal=0, antaludlaan=0, aarstal=0, instruktor="", laengde=0, kategori="Film"):
        super().__init__(idnr, titel, antal, antaludlaan, aarstal, kategori)
        self.instruktor = instruktor
        self.laengde = laengde

    def toString(self):
        text = f"ID: {self.idnr:<5} Titel({self.kategori}): {self.titel:<19} Antal: {self.antal:<5} Udlånt: {self.antaludlaan:<5} År: {self.aarstal:<8} Minutter: {self.laengde:<6} Instruktør: {self.instruktor:<}"
        return text

# Ekstra klasse, ligner meget på klassen Film, men instruktør erstattes med artist
class Cd(Materiale):
    def __init__(self, idnr=0, titel="", antal=0, antaludlaan=0, aarstal=0, artist="", laengde=0, kategori="CD"):
        super().__init__(idnr, titel, antal, antaludlaan, aarstal, kategori)
        self.artist = artist
        self.laengde = laengde

    def toString(self):
        text = f"ID: {self.idnr:<5} Titel({self.kategori}): {self.titel:<21} Antal: {self.antal:<5} Udlånt: {self.antaludlaan:<5} År: {self.aarstal:<8} Minutter: {self.laengde:<6} Artist: {self.artist:<}"
        return text