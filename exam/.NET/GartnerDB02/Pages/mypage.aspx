﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/gartner.Master" AutoEventWireup="true" CodeBehind="mypage.aspx.cs" Inherits="GartnerDB02.Pages.mypage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        width: 100%;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>List of plants for user:<asp:Label ID="UserLabel" runat="server" Text="Label"></asp:Label>
</h1>
    <table class="auto-style1">
        </table>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <br />
                        <asp:Button ID="ButtonShowPLants" runat="server" OnClick="ButtonShowPLants_Click" Text="Show My Plants" />
                        <br />
                        <br />
                        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                        <br />
                        <br />
                        <asp:GridView ID="GridMyPlants" runat="server">
                        </asp:GridView>
                        <br />
                        <hr />
                        <br />
                        Search for plants:
                        <asp:TextBox ID="SearchBox" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                        &nbsp;<asp:Button ID="SearchButton" runat="server" OnClick="Button2_Click" Text="Search" />
                        <br />
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                        <br />
                        <asp:GridView ID="SearchResult" runat="server">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkCtrl" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <br />
                        Add or update plants. I have
                        <asp:TextBox ID="NumberBox" runat="server"></asp:TextBox>
                        
                        &nbsp;number of plants<br /> &nbsp;
                        <asp:Button ID="AddButton" runat="server" EnableViewState="False" OnClick="Button1_Click" Text="Add" Width="38px" />
                        &nbsp;
                        <asp:Button ID="Update" runat="server" OnClick="Update_Click" Text="Update" />
                        <asp:LoginStatus ID="LoginStatus1" runat="server" LoginText="" />
                        <br />
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" 
                        ControlToValidate="NumberBox" ErrorMessage="You can only use whole numbers when updating or adding to your list" />
                    </ContentTemplate>
                </asp:UpdatePanel>
    </asp:Content>
