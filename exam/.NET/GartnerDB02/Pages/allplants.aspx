﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/gartner.Master" AutoEventWireup="true" CodeBehind="allplants.aspx.cs" Inherits="GartnerDB02.Pages.allplants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="PlantId" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:BoundField DataField="PlantId" HeaderText="PlantId" ReadOnly="True" SortExpression="PlantId" />
            <asp:BoundField DataField="LatinName" HeaderText="LatinName" SortExpression="LatinName" />
            <asp:BoundField DataField="DanishName" HeaderText="DanishName" SortExpression="DanishName" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [PlantId], [LatinName], [DanishName] FROM [Plants]"></asp:SqlDataSource>
</asp:Content>
