﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/gartner.Master" AutoEventWireup="true" CodeBehind="allusers.aspx.cs" Inherits="GartnerDB02.Pages.admin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
    <Columns>
        <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
        <asp:BoundField DataField="LatinName" HeaderText="LatinName" SortExpression="LatinName" />
        <asp:BoundField DataField="DanishName" HeaderText="DanishName" SortExpression="DanishName" />
        <asp:BoundField DataField="Number" HeaderText="Number" SortExpression="Number" />
    </Columns>
</asp:GridView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT Users.UserName, Plants.LatinName, Plants.DanishName, MyList.Number FROM MyList INNER JOIN Plants ON Plants.PlantId = MyList.PlantId INNER JOIN Users ON Users.UserName = MyList.UserName"></asp:SqlDataSource>
</asp:Content>
