﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using GartnerDB02.Class;

namespace GartnerDB02.Pages
{
    public partial class mypage : System.Web.UI.Page
    {
        public SqlConnection con;
        public string constr;
        string LoggedInUser = HttpContext.Current.User.Identity.Name;

        public void connection()
        {
            constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            con = new SqlConnection(constr);
            con.Open();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Visible = false;
            Label2.Visible = false;
            UserLabel.Text = HttpContext.Current.User.Identity.Name;
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        // search for plants
        protected void Button2_Click(object sender, EventArgs e)
        {
            connection();
            string query = "select DanishName from Plants where DanishName like'%" + SearchBox.Text + "%'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader dr;
            dr = com.ExecuteReader();


            if (dr.HasRows)
            {
                dr.Read();

                rep_bind();
                SearchResult.Visible = true;

                SearchBox.Text = "";
                Label1.Text = "";
            }
            else
            {
                SearchResult.Visible = false;
                Label1.Visible = true;
                Label1.Text = "There is no plant called " + SearchBox.Text + " &nbsp;In our database"; ;

            }
        }

        private void rep_bind()
        {
            connection();
            string query = "select * from Plants where DanishName like '%" + SearchBox.Text + "%'";
            SqlDataAdapter da = new SqlDataAdapter(query, con);
            DataSet ds = new DataSet();
            da.Fill(ds);
            SearchResult.DataSource = ds;
            SearchResult.DataBind();
        }

        private void rep_bind2()
        {
            connection();
            string query2 = "SELECT Plants.LatinName, Plants.DanishName, MyList.Number FROM MyList INNER JOIN Plants ON Plants.PlantId = MyList.PlantId WHERE MyList.UserName like'" + LoggedInUser + "'";
            SqlDataAdapter da2 = new SqlDataAdapter(query2, con);
            DataSet ds2 = new DataSet();
            da2.Fill(ds2);
            GridMyPlants.DataSource = ds2;
            GridMyPlants.DataBind();
        }

        // add to list
        protected void Button1_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in SearchResult.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkCtrl") as CheckBox);
                    if (chkRow.Checked)
                    {
                        string PlantId = row.Cells[1].Text;
                        string LatinName = row.Cells[2].Text;
                        string DanishName = row.Cells[3].Text;

                       

                        connection();
                        string query = "insert into MyList (UserName, PlantId, Number) values (@loggedinuser, @plantid, @number)";
                        SqlCommand com = new SqlCommand(query, con);
                        com.Parameters.AddWithValue("@loggedinuser", LoggedInUser);
                        com.Parameters.AddWithValue("@plantid", PlantId);
                        com.Parameters.AddWithValue("@number", NumberBox.Text);
                        com.ExecuteNonQuery();

                    }
                }
            }
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void ButtonShowPLants_Click(object sender, EventArgs e)
        {
            connection();
            string query = "SELECT Plants.LatinName, Plants.DanishName, MyList.Number FROM MyList INNER JOIN Plants ON Plants.PlantId = MyList.PlantId WHERE MyList.UserName like'" + LoggedInUser + "'";
            SqlCommand com = new SqlCommand(query, con);

            SqlDataReader dr2;
            dr2 = com.ExecuteReader();

            if (dr2.HasRows)
            {
                dr2.Read();

                rep_bind2();
                GridMyPlants.Visible = true;

                Label2.Text = "";
            }
            else
            {
                GridMyPlants.Visible = false;
                Label2.Visible = true;
                Label2.Text = "User " + LoggedInUser + " has no plants"; ;

            }
        }

        //update list
        protected void Update_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in SearchResult.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkCtrl") as CheckBox);
                    if (chkRow.Checked)
                    {
                        int PlantId = Int16.Parse(row.Cells[1].Text);
                        string PlantIdString = row.Cells[1].Text;
                        int NumberPlants = Int16.Parse(NumberBox.Text);
                        string LatinName = row.Cells[2].Text;
                        string DanishName = row.Cells[3].Text;



                        connection();
                        string query = "update MyList set Number= '"+NumberBox.Text + "' where UserName = '" +LoggedInUser + "' and PlantId = '" +PlantIdString + "'";
                        SqlCommand com = new SqlCommand(query, con);
                        com.ExecuteNonQuery();

                    }
                }
            }
        }
    }
}