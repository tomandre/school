﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace GartnerDB02.Class
{
    public class Connection
    {
        public SqlConnection con;
        public string constr;
        string LoggedInUser = HttpContext.Current.User.Identity.Name;
        string query = "";
        


        public void connection()
        {
            constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            con = new SqlConnection(constr);
            con.Open();
        }

        public void read()
        {
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader dr;
            dr = com.ExecuteReader();
        }
    }
}