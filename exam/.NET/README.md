# Plant database using .NET/C#/SQL

![Login](login.png)
![My plants](myplants.png)


Project where you can keep track of what plants you have, and how many. For now, the number of plants is just an example. In the future "number" should be replaced with for instance area in size or actual number. For now, the project includes:

- My plants, with an option to add or remove
- All users, a list of all users and their plants
- All plants, a list of all plant names in the database

